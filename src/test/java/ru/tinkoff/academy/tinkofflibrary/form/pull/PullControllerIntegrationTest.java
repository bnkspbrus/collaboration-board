package ru.tinkoff.academy.tinkofflibrary.form.pull;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import ru.tinkoff.academy.tinkofflibrary.AbstractIntegrationTest;
import ru.tinkoff.academy.tinkofflibrary.form.pull.dto.PullDto;

import java.util.Random;
import java.util.stream.LongStream;
import java.util.stream.Stream;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@AutoConfigureMockMvc
@WithMockUser(username = "admin", roles = "ADMIN")
public class PullControllerIntegrationTest extends AbstractIntegrationTest {
    @Autowired
    private PullRepository pullRepository;
    @Autowired
    private MockMvc mvc;

    @ParameterizedTest
    @MethodSource("getTestGetBoardParams")
    @Order(1)
    public void testGetPull(long id, PullDto expectedPullDto) throws Exception {
        MvcResult response = mvc
                .perform(get("/pull/" + id).accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andReturn();
        PullDto actualPullDto = mapToPullDto(response.getResponse().getContentAsString());
        Assertions.assertEquals(expectedPullDto, actualPullDto);
    }

    @ParameterizedTest
    @MethodSource("getTestCreatePullParams")
    @Order(2)
    public void testCreatePull(PullDto pullDto) throws Exception {
        MvcResult response = mvc.perform(post("/pull")
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
                        .content(getObjectMapper().writeValueAsString(pullDto)))
                .andExpect(status().isOk())
                .andReturn();
        PullDto actualBoardDto = mapToPullDto(response.getResponse().getContentAsString());
        Assertions.assertEquals(pullDto, actualBoardDto);
    }

    @ParameterizedTest
    @MethodSource("getTestUpdatePullParams")
    @Order(3)
    public void testUpdatePull(long id, PullDto pullDto) throws Exception {
        MvcResult response = mvc.perform(put("/pull/" + id)
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
                        .content(getObjectMapper().writeValueAsString(pullDto)))
                .andExpect(status().isOk())
                .andReturn();
        PullDto actualPullDto = mapToPullDto(response.getResponse().getContentAsString());
        Assertions.assertEquals(pullDto, actualPullDto);
    }

    @ParameterizedTest
    @MethodSource("getTestDeletePullParams")
    @Order(4)
    public void testRemovePull(long id) throws Exception {
        mvc.perform(delete("/pull/" + id)).andExpect(status().isOk());
        Assertions.assertFalse(pullRepository.existsById(id));
    }

    private static LongStream getTestDeletePullParams() {
        return LongStream.rangeClosed(6, 10);
    }

    private ObjectMapper getObjectMapper() {
        ObjectMapper mapper = new ObjectMapper();
        mapper.registerModule(new JavaTimeModule());
        mapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
        return mapper;
    }

    private PullDto mapToPullDto(String pullJson) throws JsonProcessingException {
        Pull pull = getObjectMapper().readValue(pullJson, Pull.class);
        return new ModelMapper().map(pull, PullDto.class);
    }

    private static Stream<Arguments> getTestUpdatePullParams() {
        Random random = new Random();
        return Stream.generate(() -> Arguments.of(
                6 + random.nextLong(5),
                getRandomPullDto())
        ).limit(5);
    }

    private static Stream<PullDto> getTestCreatePullParams() {
        return Stream.generate(PullControllerIntegrationTest::getRandomPullDto).limit(5);
    }

    private static PullDto getRandomPullDto() {
        return PullDto.builder()
                .name(RandomStringUtils.randomAlphanumeric(4, 10))
                .description(RandomStringUtils.randomAlphanumeric(4, 10))
                .build();
    }

    private static Stream<Arguments> getTestGetBoardParams() {
        return Stream.of(
                Arguments.of(1L, PullDto.builder()
                        .name("pull_1")
                        .description("desc_1")
                        .build()),
                Arguments.of(2L, PullDto.builder()
                        .name("pull_2")
                        .description("desc_2")
                        .build()),
                Arguments.of(3L, PullDto.builder()
                        .name("pull_2")
                        .description("desc_2")
                        .build()),
                Arguments.of(4L, PullDto.builder()
                        .name("pull_2")
                        .description("desc_2")
                        .build()),
                Arguments.of(5L, PullDto.builder()
                        .name("pull_2")
                        .description("desc_2")
                        .build())
        );
    }
}

