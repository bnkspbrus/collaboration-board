package ru.tinkoff.academy.tinkofflibrary.form.note;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import ru.tinkoff.academy.tinkofflibrary.AbstractIntegrationTest;
import ru.tinkoff.academy.tinkofflibrary.form.note.dto.NoteDto;

import java.time.LocalDateTime;
import java.util.Random;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.LongStream;
import java.util.stream.Stream;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@AutoConfigureMockMvc
@WithMockUser(username = "admin", roles = "ADMIN")
public class NoteControllerIntegrationTest extends AbstractIntegrationTest {
    @Autowired
    private NoteRepository noteRepository;
    @Autowired
    private MockMvc mvc;

    @ParameterizedTest
    @MethodSource("getTestGetNoteParams")
    @Order(1)
    public void testGetNote(long id, NoteDto expectedNoteDto) throws Exception {
        MvcResult response = mvc
                .perform(get("/note/" + id).accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andReturn();
        NoteDto actualNoteDto = mapToNoteDto(response.getResponse().getContentAsString());
        Assertions.assertEquals(expectedNoteDto, actualNoteDto);
    }

    @ParameterizedTest
    @MethodSource("getTestCreateNoteParams")
    @Order(2)
    public void testCreateNote(NoteDto noteDto) throws Exception {
        MvcResult response = mvc.perform(post("/note")
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
                        .content(getObjectMapper().writeValueAsString(noteDto)))
                .andExpect(status().isOk())
                .andReturn();
        NoteDto actualNoteDto = mapToNoteDto(response.getResponse().getContentAsString());
        Assertions.assertEquals(noteDto, actualNoteDto);
    }

    @ParameterizedTest
    @MethodSource("getTestUpdateNoteParams")
    @Order(3)
    public void testUpdateNote(long id, NoteDto noteDto) throws Exception {
        MvcResult response = mvc.perform(put("/note/" + id)
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
                        .content(getObjectMapper().writeValueAsString(noteDto)))
                .andExpect(status().isOk())
                .andReturn();
        NoteDto actualNoteDto = mapToNoteDto(response.getResponse().getContentAsString());
        Assertions.assertEquals(noteDto, actualNoteDto);
    }

    @ParameterizedTest
    @MethodSource("getTestDeleteNoteParams")
    @Order(4)
    public void testRemoveForm(long id) throws Exception {
        mvc.perform(delete("/note/" + id)).andExpect(status().isOk());
        Assertions.assertFalse(noteRepository.existsById(id));
    }

    @ParameterizedTest
    @MethodSource("getTestSearchNoteParams")
    @Order(5)
    public void testSearchNote(String tag, Set<Long> expectedNoteIds) throws Exception {
        MvcResult response = mvc
                .perform(get("/note/search").param("tag", tag).accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andReturn();
        String noteSetJson = response.getResponse().getContentAsString();
        Set<Note> actualNotes = getObjectMapper().readValue(noteSetJson, new TypeReference<>() {
        });
        Set<Long> actualNoteIds = actualNotes.stream().map(Note::getId).collect(Collectors.toSet());
        Assertions.assertEquals(expectedNoteIds, actualNoteIds);
    }

    private static LongStream getTestDeleteNoteParams() {
        return LongStream.rangeClosed(6, 10);
    }

    private ObjectMapper getObjectMapper() {
        ObjectMapper mapper = new ObjectMapper();
        mapper.registerModule(new JavaTimeModule());
        mapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
        return mapper;
    }

    private NoteDto mapToNoteDto(String noteJson) throws JsonProcessingException {
        Note note = getObjectMapper().readValue(noteJson, Note.class);
        NoteDto noteDto = new ModelMapper().map(note, NoteDto.class);
        if (note.getNote() != null) {
            noteDto.setNoteId(note.getNote().getId());
        }
        if (note.getTag() != null) {
            noteDto.setTagId(note.getTag().getId());
        }
        return noteDto;
    }

    private static Stream<Arguments> getTestSearchNoteParams() {
        return Stream.of(
                Arguments.of("#a", Set.of()),
                Arguments.of("#b", Set.of(3L, 4L)),
                Arguments.of("#c", Set.of(5L)),
                Arguments.of("#d", Set.of(1L)),
                Arguments.of("#e", Set.of(2L))
        );
    }

    private static Stream<Arguments> getTestUpdateNoteParams() {
        Random random = new Random();
        return Stream.generate(() -> Arguments.of(
                6 + random.nextLong(5),
                getRandomNoteDto())
        ).limit(5);
    }

    private static Stream<NoteDto> getTestCreateNoteParams() {
        return Stream.generate(NoteControllerIntegrationTest::getRandomNoteDto).limit(5);
    }

    private static NoteDto getRandomNoteDto() {
        Random random = new Random();
        return NoteDto.builder()
                .name(RandomStringUtils.randomAlphanumeric(4, 10))
                .text(RandomStringUtils.randomAlphanumeric(4, 10))
                .noteId(1 + random.nextLong(5))
                .creationTime(LocalDateTime.parse("2222-12-22T12:12:12"))
                .modificationTime(LocalDateTime.parse("2222-12-22T12:12:12"))
                .tagId(1 + random.nextLong(5))
                .build();
    }

    private static Stream<Arguments> getTestGetNoteParams() {
        return Stream.of(
                Arguments.of(1L, NoteDto.builder()
                        .name("note_1")
                        .text("text_1")
                        .creationTime(LocalDateTime.parse("2222-12-22T12:12:12"))
                        .modificationTime(LocalDateTime.parse("2222-12-22T12:12:12"))
                        .tagId(4L)
                        .build()),
                Arguments.of(2L, NoteDto.builder()
                        .name("note_2")
                        .text("text_2")
                        .creationTime(LocalDateTime.parse("2222-12-22T12:12:12"))
                        .modificationTime(LocalDateTime.parse("2222-12-22T12:12:12"))
                        .noteId(1L)
                        .tagId(5L)
                        .build()),
                Arguments.of(3L, NoteDto.builder()
                        .name("note_3")
                        .text("text_3")
                        .creationTime(LocalDateTime.parse("2222-12-22T12:12:12"))
                        .modificationTime(LocalDateTime.parse("2222-12-22T12:12:12"))
                        .noteId(1L)
                        .tagId(2L)
                        .build()),
                Arguments.of(4L, NoteDto.builder()
                        .name("note_4")
                        .text("text_4")
                        .creationTime(LocalDateTime.parse("2222-12-22T12:12:12"))
                        .modificationTime(LocalDateTime.parse("2222-12-22T12:12:12"))
                        .noteId(3L)
                        .tagId(2L)
                        .build()),
                Arguments.of(5L, NoteDto.builder()
                        .name("note_5")
                        .text("text_5")
                        .creationTime(LocalDateTime.parse("2222-12-22T12:12:12"))
                        .modificationTime(LocalDateTime.parse("2222-12-22T12:12:12"))
                        .noteId(4L)
                        .tagId(3L)
                        .build())
        );
    }
}
