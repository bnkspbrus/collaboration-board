package ru.tinkoff.academy.tinkofflibrary.form.board.task;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.web.context.WebApplicationContext;
import ru.tinkoff.academy.tinkofflibrary.AbstractIntegrationTest;
import ru.tinkoff.academy.tinkofflibrary.form.board.task.dto.TaskDto;

import java.time.LocalDateTime;
import java.util.Random;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.LongStream;
import java.util.stream.Stream;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@AutoConfigureMockMvc
@WithMockUser(username = "admin", roles = "ADMIN")
public class TaskControllerIntegrationTest extends AbstractIntegrationTest {
    @Autowired
    private TaskRepository taskRepository;
    @Autowired
    private MockMvc mvc;

    @ParameterizedTest
    @MethodSource("getTestGetTaskParams")
    @Order(1)
    public void testGetTask(long id, TaskDto expectedTaskDto) throws Exception {
        MvcResult response = mvc
                .perform(get("/task/" + id).accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andReturn();
        TaskDto actualTaskDto = mapToTaskDto(response.getResponse().getContentAsString());
        Assertions.assertEquals(expectedTaskDto, actualTaskDto);
    }

    @ParameterizedTest
    @MethodSource("getTestCreateTaskParams")
    @Order(2)
    public void testCreateTask(TaskDto taskDto) throws Exception {
        MvcResult response = mvc.perform(post("/task")
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
                        .content(getObjectMapper().writeValueAsString(taskDto)))
                .andExpect(status().isOk())
                .andReturn();
        TaskDto actualTaskDto = mapToTaskDto(response.getResponse().getContentAsString());
        Assertions.assertEquals(taskDto, actualTaskDto);
    }

    @ParameterizedTest
    @MethodSource("getTestUpdateTaskParams")
    @Order(3)
    public void testUpdateTask(long id, TaskDto taskDto) throws Exception {
        MvcResult response = mvc.perform(put("/task/" + id)
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
                        .content(getObjectMapper().writeValueAsString(taskDto)))
                .andExpect(status().isOk())
                .andReturn();
        TaskDto actualTaskDto = mapToTaskDto(response.getResponse().getContentAsString());
        Assertions.assertEquals(taskDto, actualTaskDto);
    }

    @ParameterizedTest
    @MethodSource("getTestDeleteTaskParams")
    @Order(4)
    public void testRemoveTask(long id) throws Exception {
        mvc.perform(delete("/task/" + id)).andExpect(status().isOk());
        Assertions.assertFalse(taskRepository.existsById(id));
    }

    @ParameterizedTest
    @MethodSource("getTestSearchTaskParams")
    @Order(5)
    public void testSearchTask(String tag, Set<Long> expectedTaskIds) throws Exception {
        MvcResult response = mvc
                .perform(get("/task/search").param("tag", tag).accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andReturn();
        String taskSetJson = response.getResponse().getContentAsString();
        Set<Task> actualNotes = getObjectMapper().readValue(taskSetJson, new TypeReference<>() {
        });
        Set<Long> actualTaskIds = actualNotes.stream().map(Task::getId).collect(Collectors.toSet());
        Assertions.assertEquals(expectedTaskIds, actualTaskIds);
    }

    private static LongStream getTestDeleteTaskParams() {
        return LongStream.rangeClosed(6, 10);
    }

    private ObjectMapper getObjectMapper() {
        ObjectMapper mapper = new ObjectMapper();
        mapper.registerModule(new JavaTimeModule());
        mapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
        return mapper;
    }

    private TaskDto mapToTaskDto(String taskJson) throws JsonProcessingException {
        Task task = getObjectMapper().readValue(taskJson, Task.class);
        TaskDto taskDto = new ModelMapper().map(task, TaskDto.class);
        if (task.getBoard() != null) {
            taskDto.setBoardId(task.getBoard().getId());
        }
        if (task.getStatus() != null) {
            taskDto.setStatusId(task.getStatus().getId());
        }
        if (task.getTag() != null) {
            taskDto.setTagId(task.getTag().getId());
        }
        return taskDto;
    }

    private static Stream<Arguments> getTestSearchTaskParams() {
        return Stream.of(
                Arguments.of("#a", Set.of(1L, 3L)),
                Arguments.of("#b", Set.of(2L)),
                Arguments.of("#c", Set.of()),
                Arguments.of("#d", Set.of(4L)),
                Arguments.of("#e", Set.of(5L))
        );
    }

    private static Stream<Arguments> getTestUpdateTaskParams() {
        Random random = new Random();
        return Stream.generate(() -> Arguments.of(
                6 + random.nextLong(5),
                TaskDto.builder()
                        .name(RandomStringUtils.randomAlphanumeric(4, 10))
                        .description(RandomStringUtils.randomAlphanumeric(4, 10))
                        .creationTime(LocalDateTime.parse("2222-12-22T12:12:12"))
                        .modificationTime(LocalDateTime.parse("2222-12-22T12:12:12"))
                        .tagId(1 + random.nextLong(5))
                        .boardId(1 + random.nextLong(5))
                        .build())
        ).limit(5);
    }

    private static Stream<TaskDto> getTestCreateTaskParams() {
        Random random = new Random();
        return Stream.generate(() -> TaskDto.builder()
                .name(RandomStringUtils.randomAlphanumeric(4, 10))
                .description(RandomStringUtils.randomAlphanumeric(4, 10))
                .creationTime(LocalDateTime.parse("2222-12-22T12:12:12"))
                .modificationTime(LocalDateTime.parse("2222-12-22T12:12:12"))
                .tagId(1 + random.nextLong(5))
                .boardId(1 + random.nextLong(5))
                .build()
        ).limit(5);
    }

    private static Stream<Arguments> getTestGetTaskParams() {
        return Stream.of(
                Arguments.of(1L, TaskDto.builder()
                        .name("task_1")
                        .description("desc_1")
                        .creationTime(LocalDateTime.parse("2222-12-22T12:12:12"))
                        .modificationTime(LocalDateTime.parse("2222-12-22T12:12:12"))
                        .tagId(1L)
                        .statusId(1L)
                        .boardId(1L)
                        .build()),
                Arguments.of(2L, TaskDto.builder()
                        .name("task_2")
                        .description("desc_2")
                        .creationTime(LocalDateTime.parse("2222-12-22T12:12:12"))
                        .modificationTime(LocalDateTime.parse("2222-12-22T12:12:12"))
                        .tagId(2L)
                        .statusId(3L)
                        .boardId(4L)
                        .build()),
                Arguments.of(3L, TaskDto.builder()
                        .name("task_3")
                        .description("desc_3")
                        .creationTime(LocalDateTime.parse("2222-12-22T12:12:12"))
                        .modificationTime(LocalDateTime.parse("2222-12-22T12:12:12"))
                        .tagId(1L)
                        .statusId(3L)
                        .boardId(1L)
                        .build()),
                Arguments.of(4L, TaskDto.builder()
                        .name("task_4")
                        .description("desc_4")
                        .creationTime(LocalDateTime.parse("2222-12-22T12:12:12"))
                        .modificationTime(LocalDateTime.parse("2222-12-22T12:12:12"))
                        .tagId(4L)
                        .statusId(5L)
                        .boardId(1L)
                        .build()),
                Arguments.of(5L, TaskDto.builder()
                        .name("task_5")
                        .description("desc_5")
                        .creationTime(LocalDateTime.parse("2222-12-22T12:12:12"))
                        .modificationTime(LocalDateTime.parse("2222-12-22T12:12:12"))
                        .tagId(5L)
                        .statusId(5L)
                        .boardId(2L)
                        .build())
        );
    }
}

