package ru.tinkoff.academy.tinkofflibrary.form.board;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.jupiter.api.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import ru.tinkoff.academy.tinkofflibrary.AbstractIntegrationTest;
import ru.tinkoff.academy.tinkofflibrary.form.board.dto.BoardDto;
import ru.tinkoff.academy.tinkofflibrary.form.board.status.Status;

import java.time.LocalDateTime;
import java.util.Random;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.LongStream;
import java.util.stream.Stream;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@AutoConfigureMockMvc
@WithMockUser(username = "admin", roles = "ADMIN")
public class BoardControllerIntegrationTest extends AbstractIntegrationTest {
    @Autowired
    private BoardRepository boardRepository;
    @Autowired
    private MockMvc mvc;

    @ParameterizedTest
    @MethodSource("getTestGetBoardParams")
    @Order(1)
    public void testGetBoard(long id, BoardDto expectedBoardDto) throws Exception {
        MvcResult response = mvc
                .perform(get("/board/" + id).accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andReturn();
        BoardDto actualBoardDto = mapToBoardDto(response.getResponse().getContentAsString());
        Assertions.assertEquals(expectedBoardDto, actualBoardDto);
    }

    @ParameterizedTest
    @MethodSource("getTestCreateBoardParams")
    @Order(2)
    public void testCreateBoard(BoardDto boardDto) throws Exception {
        MvcResult response = mvc.perform(post("/board")
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
                        .content(getObjectMapper().writeValueAsString(boardDto)))
                .andExpect(status().isOk())
                .andReturn();
        BoardDto actualBoardDto = mapToBoardDto(response.getResponse().getContentAsString());
        Assertions.assertEquals(boardDto, actualBoardDto);
    }

    @ParameterizedTest
    @MethodSource("getTestUpdateBoardParams")
    @Order(3)
    public void testUpdateBoard(long id, BoardDto boardDto) throws Exception {
        MvcResult response = mvc.perform(put("/board/" + id)
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
                        .content(getObjectMapper().writeValueAsString(boardDto)))
                .andExpect(status().isOk())
                .andReturn();
        BoardDto actualBoardDto = mapToBoardDto(response.getResponse().getContentAsString());
        Assertions.assertEquals(boardDto, actualBoardDto);
    }

    @ParameterizedTest
    @MethodSource("getTestDeleteBoardParams")
    @Order(4)
    public void testRemoveBoard(long id) throws Exception {
        mvc.perform(delete("/board/" + id)).andExpect(status().isOk());
        Assertions.assertFalse(boardRepository.existsById(id));
    }

    private static LongStream getTestDeleteBoardParams() {
        return LongStream.rangeClosed(6, 10);
    }

    private ObjectMapper getObjectMapper() {
        ObjectMapper mapper = new ObjectMapper();
        mapper.registerModule(new JavaTimeModule());
        mapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
        return mapper;
    }

    private BoardDto mapToBoardDto(String boardJson) throws JsonProcessingException {
        Board board = getObjectMapper().readValue(boardJson, Board.class);
        BoardDto boardDto = new ModelMapper().map(board, BoardDto.class);
        if (board.getStatuses() != null) {
            boardDto.setStatusIds(board.getStatuses().stream().map(Status::getId).collect(Collectors.toSet()));
        }
        return boardDto;
    }

    private static Stream<Arguments> getTestUpdateBoardParams() {
        Random random = new Random();
        return Stream.generate(() -> Arguments.of(
                6 + random.nextLong(5),
                getRandomBoardDto())
        ).limit(5);
    }

    private static Stream<BoardDto> getTestCreateBoardParams() {
        return Stream.generate(BoardControllerIntegrationTest::getRandomBoardDto).limit(5);
    }

    private static BoardDto getRandomBoardDto() {
        return BoardDto.builder()
                .name(RandomStringUtils.randomAlphanumeric(4, 10))
                .description(RandomStringUtils.randomAlphanumeric(4, 10))
                .creationTime(LocalDateTime.parse("2222-12-22T12:12:12"))
                .modificationTime(LocalDateTime.parse("2222-12-22T12:12:12"))
                .statusIds(
                        new Random()
                                .longs(5, 1, 6)
                                .boxed()
                                .collect(Collectors.toSet())
                )
                .build();
    }

    private static Stream<Arguments> getTestGetBoardParams() {
        return Stream.of(
                Arguments.of(1L, BoardDto.builder()
                        .name("board_1")
                        .description("desc_1")
                        .creationTime(LocalDateTime.parse("2000-10-20T10:10:10"))
                        .modificationTime(LocalDateTime.parse("2222-12-22T12:12:12"))
                        .statusIds(Set.of(1L, 5L))
                        .build()),
                Arguments.of(2L, BoardDto.builder()
                        .name("board_2")
                        .description("desc_2")
                        .creationTime(LocalDateTime.parse("2000-10-20T10:10:10"))
                        .modificationTime(LocalDateTime.parse("2222-12-22T12:12:12"))
                        .statusIds(Set.of(1L, 2L, 4L))
                        .build()),
                Arguments.of(3L, BoardDto.builder()
                        .name("board_3")
                        .description("desc_3")
                        .creationTime(LocalDateTime.parse("2000-10-20T10:10:10"))
                        .modificationTime(LocalDateTime.parse("2222-12-22T12:12:12"))
                        .statusIds(Set.of())
                        .build()),
                Arguments.of(4L, BoardDto.builder()
                        .name("board_4")
                        .description("desc_4")
                        .creationTime(LocalDateTime.parse("2000-10-20T10:10:10"))
                        .modificationTime(LocalDateTime.parse("2222-12-22T12:12:12"))
                        .statusIds(Set.of(4L))
                        .build()),
                Arguments.of(5L, BoardDto.builder()
                        .name("board_5")
                        .description("desc_5")
                        .creationTime(LocalDateTime.parse("2000-10-20T10:10:10"))
                        .modificationTime(LocalDateTime.parse("2222-12-22T12:12:12"))
                        .statusIds(Set.of(1L, 4L))
                        .build())
        );
    }
}
