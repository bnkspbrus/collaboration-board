package ru.tinkoff.academy.tinkofflibrary.form;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import ru.tinkoff.academy.tinkofflibrary.AbstractIntegrationTest;
import ru.tinkoff.academy.tinkofflibrary.form.dto.FormDto;

import java.io.IOException;
import java.util.stream.LongStream;
import java.util.stream.Stream;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@AutoConfigureMockMvc
@WithMockUser(username = "admin", roles = "ADMIN")
public class FormControllerIntegrationTest extends AbstractIntegrationTest {
    @Autowired
    private FormRepository formRepository;
    @Autowired
    private MockMvc mvc;

    @ParameterizedTest
    @MethodSource("getTestGetFormParams")
    @Order(1)
    public void testGetForm(long id, FormDto expectedFormDto) throws Exception {
        MvcResult response = mvc
                .perform(get("/form/" + id).accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andReturn();
        FormDto actualFormDto = mapToFormDto(response.getResponse().getContentAsString());
        Assertions.assertEquals(expectedFormDto, actualFormDto);
    }

    @ParameterizedTest
    @MethodSource("getTestCreateFormParams")
    @Order(2)
    public void testCreateForm(FormDto formDto) throws Exception {
        MvcResult response = mvc.perform(post("/form")
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
                        .content(getObjectMapper().writeValueAsString(formDto)))
                .andExpect(status().isOk())
                .andReturn();
        FormDto actualFormDto = mapToFormDto(response.getResponse().getContentAsString());
        Assertions.assertEquals(formDto, actualFormDto);
    }

    @ParameterizedTest
    @MethodSource("getTestUpdateFormParams")
    @Order(3)
    public void testUpdateForm(long id, FormDto formDto) throws Exception {
        MvcResult response = mvc.perform(put("/form/" + id)
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
                        .content(getObjectMapper().writeValueAsString(formDto)))
                .andExpect(status().isOk())
                .andReturn();
        FormDto actualFormDto = mapToFormDto(response.getResponse().getContentAsString());
        Assertions.assertEquals(formDto, actualFormDto);
    }

    @ParameterizedTest
    @MethodSource("getTestDeleteFormParams")
    @Order(4)
    public void testRemoveForm(long id) throws Exception {
        mvc.perform(delete("/form/" + id)).andExpect(status().isOk());
        Assertions.assertFalse(formRepository.existsById(id));
    }

    private ObjectMapper getObjectMapper() {
        ObjectMapper mapper = new ObjectMapper();
        mapper.registerModule(new JavaTimeModule());
        mapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
        return mapper;
    }

    private FormDto mapToFormDto(String formJson) throws IOException {
        //ObjectMapper mapper = getObjectMapper();
        Form form = getObjectMapper().readValue(formJson, Form.class);
        FormDto formDto = new ModelMapper().map(form, FormDto.class);
        if (form.getBoard() != null) {
            formDto.setBoardId(form.getBoard().getId());
        }
        if (form.getNote() != null) {
            formDto.setNoteId(form.getNote().getId());
        }
        return formDto;
    }

    private static LongStream getTestDeleteFormParams() {
        return LongStream.rangeClosed(11, 17);
    }

    private static Stream<Arguments> getTestUpdateFormParams() {
        return Stream.of(
                Arguments.of(11L, FormDto.builder().formType(FormType.BOARD).boardId(2L).build()),
                Arguments.of(12L, FormDto.builder().formType(FormType.BOARD).boardId(2L).build()),
                Arguments.of(13L, FormDto.builder().formType(FormType.NOTE).noteId(5L).build()),
                Arguments.of(14L, FormDto.builder().formType(FormType.BOARD).boardId(5L).build()),
                Arguments.of(15L, FormDto.builder().formType(FormType.NOTE).noteId(1L).build()),
                Arguments.of(16L, FormDto.builder().formType(FormType.NOTE).noteId(4L).build()),
                Arguments.of(17L, FormDto.builder().formType(FormType.BOARD).boardId(4L).build())
        );
    }

    private static Stream<FormDto> getTestCreateFormParams() {
        return Stream.of(
                FormDto.builder().formType(FormType.BOARD).boardId(1L).build(),
                FormDto.builder().formType(FormType.NOTE).noteId(2L).build(),
                FormDto.builder().formType(FormType.NOTE).noteId(3L).build(),
                FormDto.builder().formType(FormType.BOARD).boardId(5L).build(),
                FormDto.builder().formType(FormType.BOARD).boardId(1L).build(),
                FormDto.builder().formType(FormType.NOTE).noteId(4L).build(),
                FormDto.builder().formType(FormType.BOARD).boardId(4L).build()
        );
    }

    private static Stream<Arguments> getTestGetFormParams() {
        return Stream.of(
                Arguments.of(1L, FormDto.builder().formType(FormType.BOARD).boardId(1L).build()),
                Arguments.of(2L, FormDto.builder().formType(FormType.NOTE).noteId(1L).build()),
                Arguments.of(3L, FormDto.builder().formType(FormType.NOTE).noteId(2L).build()),
                Arguments.of(4L, FormDto.builder().formType(FormType.BOARD).boardId(2L).build()),
                Arguments.of(5L, FormDto.builder().formType(FormType.NOTE).noteId(3L).build()),
                Arguments.of(6L, FormDto.builder().formType(FormType.BOARD).boardId(5L).build()),
                Arguments.of(7L, FormDto.builder().formType(FormType.NOTE).noteId(3L).build()),
                Arguments.of(8L, FormDto.builder().formType(FormType.BOARD).boardId(4L).build()),
                Arguments.of(9L, FormDto.builder().formType(FormType.NOTE).noteId(5L).build()),
                Arguments.of(10L, FormDto.builder().formType(FormType.NOTE).noteId(2L).build())
        );
    }
}
