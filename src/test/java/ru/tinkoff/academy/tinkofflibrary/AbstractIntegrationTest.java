package ru.tinkoff.academy.tinkofflibrary;

import org.springframework.test.context.DynamicPropertyRegistry;
import org.springframework.test.context.DynamicPropertySource;
import org.testcontainers.containers.PostgreSQLContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;

@Testcontainers
public abstract class AbstractIntegrationTest {
    private static final String IMAGE_VERSION = "postgres:latest";
    @SuppressWarnings("rawtypes")
    @Container
    protected static final PostgreSQLContainer CONTAINER;

    static {
        //noinspection rawtypes
        CONTAINER = new PostgreSQLContainer(IMAGE_VERSION) {
            @Override
            public void stop() {
            }
        };
        CONTAINER.start();
    }

    @DynamicPropertySource
    static void registerPostgresProperties(DynamicPropertyRegistry registry) {
        registry.add("spring.datasource.url", CONTAINER::getJdbcUrl);
        registry.add("spring.datasource.username", CONTAINER::getUsername);
        registry.add("spring.datasource.password", CONTAINER::getPassword);
    }
}
