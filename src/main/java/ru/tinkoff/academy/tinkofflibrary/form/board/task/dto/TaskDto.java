package ru.tinkoff.academy.tinkofflibrary.form.board.task.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.tinkoff.academy.tinkofflibrary.form.board.Board;
import ru.tinkoff.academy.tinkofflibrary.form.board.status.Status;

import javax.persistence.CascadeType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import java.time.LocalDateTime;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class TaskDto {
    String name;
    String description;
    private LocalDateTime creationTime;
    private LocalDateTime modificationTime;
    private Long tagId;
    private Long statusId;
    private Long boardId;

}
