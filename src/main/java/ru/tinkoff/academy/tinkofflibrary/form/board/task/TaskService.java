package ru.tinkoff.academy.tinkofflibrary.form.board.task;

import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;
import ru.tinkoff.academy.tinkofflibrary.form.board.Board;
import ru.tinkoff.academy.tinkofflibrary.form.board.BoardService;
import ru.tinkoff.academy.tinkofflibrary.form.board.status.StatusService;
import ru.tinkoff.academy.tinkofflibrary.form.board.task.TaskRepository;
import ru.tinkoff.academy.tinkofflibrary.form.board.task.dto.TaskDto;
import ru.tinkoff.academy.tinkofflibrary.form.note.Note;
import ru.tinkoff.academy.tinkofflibrary.tag.TagService;

@Service
@RequiredArgsConstructor
public class TaskService {
    private final TaskRepository taskRepository;
    private final BoardService boardService;
    private final StatusService statusService;
    private final TagService tagService;

    public Task findById(Long id) {
        return taskRepository.findById(id).orElseThrow(
                () -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Task with id " + id + " not found")
        );
    }

    public Task updateById(Long id, TaskDto taskDto) {
        if (!taskRepository.existsById(id)) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Task with id " + id + " not found");
        }
        Task task = mapToTask(taskDto);
        checkStatusCompatibility(task, task.getBoard());
        task.setId(id);
        return taskRepository.save(task);
    }

    public Task save(TaskDto taskDto) {
        Task task = mapToTask(taskDto);
        checkStatusCompatibility(task, task.getBoard());
        return taskRepository.save(task);
    }

    public void deleteById(Long id) {
        taskRepository.deleteById(id);
    }

    private Task mapToTask(TaskDto taskDto) {
        ModelMapper mapper = new ModelMapper();
        mapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);
        Task task = mapper.map(taskDto, Task.class);
        if (taskDto.getBoardId() != null) {
            task.setBoard(boardService.findById(taskDto.getBoardId()));
        }
        if (taskDto.getStatusId() != null) {
            task.setStatus(statusService.findById(taskDto.getStatusId()));
        }
        if (taskDto.getTagId() != null) {
            task.setTag(tagService.findById(taskDto.getTagId()));
        }
        return task;
    }

    public void migrate(Long taskId, Long boardId) {
        Task task = findById(taskId);
        Board board = boardService.findById(boardId);
        checkStatusCompatibility(task, board);
        task.setBoard(board);
        taskRepository.save(task);
    }

    private void checkStatusCompatibility(Task task, Board board) {
        if (task.getStatus() != null && !board.getStatuses().contains(task.getStatus())) {
            throw new ResponseStatusException(
                    HttpStatus.BAD_REQUEST,
                    "Status with id " + task.getStatus().getId() + " not allowed for board with id " + board.getId()
            );
        }
    }
}
