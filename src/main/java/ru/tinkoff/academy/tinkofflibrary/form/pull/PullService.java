package ru.tinkoff.academy.tinkofflibrary.form.pull;

import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;
import ru.tinkoff.academy.tinkofflibrary.form.board.Board;
import ru.tinkoff.academy.tinkofflibrary.form.board.dto.BoardDto;
import ru.tinkoff.academy.tinkofflibrary.form.pull.dto.PullDto;
import ru.tinkoff.academy.tinkofflibrary.form.pull.option.OptionService;

import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class PullService {

    private final PullRepository pullRepository;

    public Pull findById(Long pullId) {
        return pullRepository.findById(pullId).orElseThrow(
                () -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Pull with id " + pullId + " not found")
        );
    }

    public Pull updateById(Long id, PullDto pullDto) {
        if (!pullRepository.existsById(id)) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Pull with id " + id + " not found");
        }
        Pull pull = mapToPull(pullDto);
        pull.setId(id);
        return pullRepository.save(pull);
    }

    public Pull save(PullDto pullDto) {
        Pull pull = mapToPull(pullDto);
        return pullRepository.save(pull);
    }

    public void deleteById(Long id) {
        pullRepository.deleteById(id);
    }

    private Pull mapToPull(PullDto pullDto) {
        return new ModelMapper().map(pullDto, Pull.class);
    }
}
