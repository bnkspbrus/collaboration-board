package ru.tinkoff.academy.tinkofflibrary.form.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.tinkoff.academy.tinkofflibrary.form.FormType;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class FormDto {
    private FormType formType;
    private Long boardId;
    private Long noteId;
    private Long pullId;
}
