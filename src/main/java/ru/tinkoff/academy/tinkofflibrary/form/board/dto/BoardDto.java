package ru.tinkoff.academy.tinkofflibrary.form.board.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.Set;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class BoardDto {
    String name;
    String description;
    private LocalDateTime creationTime;
    private LocalDateTime modificationTime;
    private Set<Long> statusIds;
}
