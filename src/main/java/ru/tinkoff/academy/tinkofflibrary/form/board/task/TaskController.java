package ru.tinkoff.academy.tinkofflibrary.form.board.task;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import ru.tinkoff.academy.tinkofflibrary.form.board.task.dto.TaskDto;
import ru.tinkoff.academy.tinkofflibrary.tag.TagService;

import java.util.Set;

@RestController
@RequiredArgsConstructor
@RequestMapping("/task")
public class TaskController {
    private final TaskService taskService;
    private final TagService tagService;

    @GetMapping(value = "/{id}", produces = "application/json")
    public Task getTask(@PathVariable Long id) {
        return taskService.findById(id);
    }

    @PutMapping(value = "/{id}", produces = "application/json", consumes = "application/json")
    public Task updateTask(@PathVariable Long id, @RequestBody TaskDto taskDto) {
        return taskService.updateById(id, taskDto);
    }

    @PostMapping(produces = "application/json", consumes = "application/json")
    public Task createTask(@RequestBody TaskDto taskDto) {
        return taskService.save(taskDto);
    }

    @DeleteMapping(value = "/{id}")
    public void removeTask(@PathVariable Long id) {
        taskService.deleteById(id);
    }

    @Operation(summary = "Search tasks by tag")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Tasks found",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = Task.class))}),
            @ApiResponse(responseCode = "404", description = "Tag not found",
                    content = @Content)})
    @GetMapping(value = "/search")
    public Set<Task> search(@RequestParam(name = "tag") String tag) {
        return tagService.findByName(tag).getTasks();
    }

    @Operation(summary = "Migrate task to another board")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Task migrated",
                    content = @Content),
            @ApiResponse(responseCode = "400", description = "Board to migrate doesn't allow status of task",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "Task/board not found",
                    content = @Content)})
    @PutMapping(value = "/migrate")
    public void migrate(@RequestParam(name = "taskId") Long taskId, @RequestParam(name = "boardId") Long boardId) {
        taskService.migrate(taskId, boardId);
    }
}
