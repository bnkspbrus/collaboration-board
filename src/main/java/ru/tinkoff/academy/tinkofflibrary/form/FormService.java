package ru.tinkoff.academy.tinkofflibrary.form;

import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;
import ru.tinkoff.academy.tinkofflibrary.form.board.BoardService;
import ru.tinkoff.academy.tinkofflibrary.form.dto.FormDto;
import ru.tinkoff.academy.tinkofflibrary.form.note.NoteService;
import ru.tinkoff.academy.tinkofflibrary.form.pull.PullService;

@Service
@RequiredArgsConstructor
public class FormService {
    private final FormRepository formRepository;
    private final BoardService boardService;
    private final NoteService noteService;
    private final PullService pullService;

    public Form findById(Long id) {
        return formRepository.findById(id).orElseThrow(
                () -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Form with id " + id + " not found")
        );
    }

    public Form save(FormDto formDto) {
        Form form = mapToForm(formDto);
        return formRepository.save(form);
    }

    public void deleteById(Long id) {
        formRepository.deleteById(id);
    }

    public Form updateById(Long id, FormDto formDto) {
        if (!formRepository.existsById(id)) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Form with id " + id + " not found");
        }
        Form form = mapToForm(formDto);
        form.setId(id);
        return formRepository.save(form);
    }

    private Form mapToForm(FormDto formDto) {
        ModelMapper mapper = new ModelMapper();
        mapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);
        Form form = mapper.map(formDto, Form.class);
        switch (form.getFormType()) {
            case BOARD -> form.setBoard(boardService.findById(formDto.getBoardId()));
            case NOTE -> form.setNote(noteService.findById(formDto.getNoteId()));
            case PULL -> form.setPull(pullService.findById(formDto.getPullId()));
        }
        return form;
    }
}
