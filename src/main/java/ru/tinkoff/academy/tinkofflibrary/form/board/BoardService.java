package ru.tinkoff.academy.tinkofflibrary.form.board;

import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;
import ru.tinkoff.academy.tinkofflibrary.form.board.dto.BoardDto;
import ru.tinkoff.academy.tinkofflibrary.form.board.status.StatusService;

import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class BoardService {
    private final BoardRepository boardRepository;
    private final StatusService statusService;

    public Board findById(Long boardId) {
        return boardRepository.findById(boardId).orElseThrow(
                () -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Board with id " + boardId + " not found")
        );
    }

    public Board updateById(Long id, BoardDto boardDto) {
        if (!boardRepository.existsById(id)) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Board with id " + id + " not found");
        }
        Board board = mapToBoard(boardDto);
        board.setId(id);
        return boardRepository.save(board);
    }

    public Board save(BoardDto boardDto) {
        Board board = mapToBoard(boardDto);
        return boardRepository.save(board);
    }

    public void deleteById(Long id) {
        boardRepository.deleteById(id);
    }

    private Board mapToBoard(BoardDto boardDto) {
        Board board = new ModelMapper().map(boardDto, Board.class);
        if (boardDto.getStatusIds() != null) {
            board.setStatuses(
                    boardDto.getStatusIds().stream().map(statusService::findById).collect(Collectors.toSet())
            );
        }
        return board;
    }
}
