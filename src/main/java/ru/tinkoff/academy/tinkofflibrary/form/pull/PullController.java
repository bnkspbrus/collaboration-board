package ru.tinkoff.academy.tinkofflibrary.form.pull;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.tinkoff.academy.tinkofflibrary.form.pull.dto.PullDto;

@RestController
@RequestMapping("/pull")
@RequiredArgsConstructor
public class PullController {
    private final PullService pullService;

    @Operation(summary = "Get pull by its id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Pull found",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = Pull.class))}),
            @ApiResponse(responseCode = "404", description = "Pull not found",
                    content = @Content)})
    @GetMapping(value = "/{id}", produces = "application/json")
    public Pull getPull(@Parameter(description = "id of pull to be gotten") @PathVariable Long id) {
        return pullService.findById(id);
    }

    @Operation(summary = "Update pull by its id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Pull updated",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = Pull.class))}),
            @ApiResponse(responseCode = "404", description = "Pull not found",
                    content = @Content)})
    @PutMapping(value = "/{id}", produces = "application/json", consumes = "application/json")
    public Pull updatePull(
            @Parameter(description = "id of pull to be updated") @PathVariable Long id,
            @RequestBody PullDto pullDto
    ) {
        return pullService.updateById(id, pullDto);
    }

    @Operation(summary = "Create new pull")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Pull created",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = Pull.class))})})
    @PostMapping(produces = "application/json", consumes = "application/json")
    public Pull createPull(@RequestBody PullDto pullDto) {
        return pullService.save(pullDto);
    }

    @Operation(summary = "Remove pull")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Pull removed",
                    content = @Content)})
    @DeleteMapping(value = "/{id}")
    public void removeBoard(@PathVariable Long id) {
        pullService.deleteById(id);
    }
}
