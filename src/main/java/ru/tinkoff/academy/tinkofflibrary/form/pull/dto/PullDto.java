package ru.tinkoff.academy.tinkofflibrary.form.pull.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Set;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class PullDto {
    private String name;
    private String description;
}
