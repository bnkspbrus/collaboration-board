package ru.tinkoff.academy.tinkofflibrary.form;

public enum FormType {
    BOARD, NOTE, PULL
}
