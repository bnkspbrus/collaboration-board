package ru.tinkoff.academy.tinkofflibrary;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;

@SpringBootApplication
public class CollaborationBoard {
    public static void main(String[] args) {
        SpringApplication.run(CollaborationBoard.class, args);
    }
}
