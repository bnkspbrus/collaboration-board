# Доска для совместной работы

ТЗ проекта: [DESCRIPTION](DESCRIPTION.md)

### Использование

* Чтобы запустить приложение запустите команду ```./gradlew bootRun```
* Чтобы подключить свою бд используйте переменые оружения `DB_URL`, `DB_USERNAME` и `DB_PASSWORD`
* Документация swagger генерится по адресу http://localhost:8080/api-docs.yaml